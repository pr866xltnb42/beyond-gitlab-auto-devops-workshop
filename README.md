# GitLab Auto DevOps Workshop

## Create your own workshop environment
These instructions are written using Docker Desktop on MacOS (Intel Chip). Other environments might work similar.

### Docker Desktop
1. Download and install Docker Desktop: https://www.docker.com/products/docker-desktop
2. Activate Kubernetes in Docker Desktop Preferences
3. Test that your cluster is up and running, e.g. using `kubectl get all --all-namespaces`

### GitLab Agent
The GitLab Agent connects the kubernetes cluster with GitLab. You can read more about it here: https://docs.gitlab.com/ee/user/clusters/agent/index.html
1. Fork this GitLab project or create a new one that you want to use for this workshop. 
2. Make sure that a file is present at `.gitlab/agents/gitlab-autodevops-workshop/config.yaml`.
3. Navigate to Infrastructure -> Kubernetes clusters
4. Click Actions -> Connect with Agent and select the agent.
5. Copy the displayed token and save it somewhere.
6. To use the recommended installation method execute the shown command, e.g.
    ```bash
   docker run --pull=always --rm \
   registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
   --agent-token=<your-token-from-last-step> \
   --kas-address=wss://kas.gitlab.com \
   --agent-version stable \
   --namespace gitlab-kubernetes-agent | kubectl apply -f -
    ```
7. You should now see a connected agent named gitlab-autodevops-workshop in GitLab.
   
### GitLab Auto DevOps
1. Go to your GitLab project and navigate to Settings -> CI/CD -> Auto DevOps.
2. Tick `Default to Auto DevOps pipeline` to enable GitLab Auto DevOps for this project. Save your changes.

### Ingress controller
To access the application via the local network later on we need to install an ingress controller to kubernetes. You can find more information about the chart here: https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx
```bash
helm upgrade --install \
--namespace ingress-nginx --create-namespace \
--repo https://kubernetes.github.io/ingress-nginx \
workshop-ingress-nginx ingress-nginx
```

### GitLab Runner
To deploy into a local kubernetes cluster we will use a local GitLab runner in it. I prepared a helm chart for a GitLab runner with full cluster access in [./gitlab-runner](./gitlab-runner). But first we need to receive a registration token.
1. Go to your GitLab project and navigate to Settings -> CI/CD -> Runners.
2. Disable shared runners for this project.
3. Copy the registration token for "Specific runners".
4. Install the GitLab runner in you local kubernetes cluster using the following commands. You can find more information here: https://docs.gitlab.com/runner/install/kubernetes.html
    ```bash
   helm dependency update ./gitlab-runner
   helm upgrade --install \
   --namespace gitlab-runner --create-namespace \
   --set gitlab-runner.runnerRegistrationToken=<your-registration-token-from-last-step> \
   workshop-gitlab-runner ./gitlab-runner
    ```
5. You should now see an available specific runner in GitLab.

### Run pipeline
Now the setup is done. You can trigger a pipeline for your project via CI/CD -> Pipelines -> Run pipeline -> Run for branch main. Then cross your fingers 🤞. Whenever you push a new commit, a new pipeline will be created automatically.

### Access deployed app
In my setup it was possible to open the environment URL right away as long as the base domain was set to `localdev.me` (see [.gitlab-ci-yaml](./.gitlab-ci.yml)). The full url is printed to the GitLab Deploy Job output or can be found in GitLab at Deployments -> Environments. For my main branch it looks like this: http://christophwalter-gitlab-autodevops-workshop.localdev.me/

If this does not work, you might need to forward the port of the kubernetes ingress controller or take a look at the [ingress-nginx documentation](https://kubernetes.github.io/ingress-nginx/deploy/#local-testing). It might also help to restart Docker Desktop or wait for a while.
```bash
kubectl port-forward -n ingress-nginx service/workshop-ingress-nginx-controller 8080:80
```

## Local Development
### Build and run the workshop app manually without kubernetes
```bash
docker build -t autodevops-workshop-app .
docker run --rm -d -p 8080:80 --name web autodevops-workshop-app
open http://localhost:8080
 ```
